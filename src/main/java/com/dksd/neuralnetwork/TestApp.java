package com.dksd.neuralnetwork;

/*
Cycling data test app.
 */
public class TestApp {

  public static void main(String[] args) {

    //inputs hidden and outputs -->
    //this seets the size of the layers and gene

    //prev_vel, vel, prev_slope, slope -> output watts
    Network network = new Network(2, 5, 1);
    double[] inputs = new double[2];
    double[] weights = new double[network.getWeightLength()];
    double [] result = network.runNetwork(inputs, weights);
    for (double v : result) {
      System.out.println("XOR problem output: " + v);
    }
  }
}
