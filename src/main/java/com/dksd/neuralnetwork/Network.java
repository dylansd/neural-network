package com.dksd.neuralnetwork;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ForkJoinPool;

/**
 * Created by dscottdawkins on 6/19/17.
 */

public class Network {

    private final int inputSize;
    private double[] hiddens;
    private double[] outputs;

    public Network(int inputSize, int hiddenSize, int outputSize) {
        this.inputSize = inputSize;
        hiddens = new double[hiddenSize];
        outputs = new double[outputSize];
    }

    public int getWeightLength() {
        return inputSize * hiddens.length + hiddens.length * outputs.length;
    }

    public double[] runNetwork(double[] inputs, double[] weights) {
        int indx = 0;
        for (int i = 0 ; i < hiddens.length; i++) {
            double hid = 0;
            for (double input : inputs) {
                hid += input * weights[indx++];
            }
            hiddens[i] = sigmoid(hid);
        }
        for (int i = 0 ; i < outputs.length; i++) {
            double hid = 0;
            for (double hidden : hiddens) {
                hid += hidden * weights[indx++];
            }
            outputs[i] = sigmoid(hid);
        }
        return outputs;
    }

    private double sigmoid(double x) {
        return 1 / ( 1 + Math.pow(Math.E,(-x)));
    }

}
